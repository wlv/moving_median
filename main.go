package main

import (
	"fmt"
	"gitlab.com/wlv/moving_median/moving_median"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {

	h := moving_median.New()
	contents, err := ioutil.ReadFile("input.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	integers := strings.Split(string(contents), "\n")[1:]
	for i := 0; i < len(integers); i++ {
		temp, err := strconv.Atoi(integers[i])

		fmt.Println(temp)
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Println("Median:")
		fmt.Println(h.Update(temp))
		fmt.Println()
	}
}
