package moving_median

import (
	"container/heap"
)

type intMinHeap []int

func (h intMinHeap) Len() int           { return len(h) }
func (h intMinHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h intMinHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h intMinHeap) Peek() int { return h[0] }

func (h *intMinHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *intMinHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type intMaxHeap []int

func (h intMaxHeap) Len() int           { return len(h) }
func (h intMaxHeap) Less(i, j int) bool { return h[i] > h[j] }
func (h intMaxHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h intMaxHeap) Peek() int { return h[0] }

func (h *intMaxHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *intMaxHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type IntMedianHeap struct {
	max *intMaxHeap
	min *intMinHeap
}

func New() *IntMedianHeap {
	h := &IntMedianHeap{
		max: &intMaxHeap{},
		min: &intMinHeap{},
	}
	heap.Init(h.max)
	heap.Init(h.min)

	return h
}

func (h IntMedianHeap) Add(n interface{}) {
	if h.max.Len() == 0 {
		heap.Push(h.max, n)
		return
	}

	if n.(int) > h.max.Peek() {
		heap.Push(h.min, n)
	} else {
		heap.Push(h.max, n)
	}

	if h.max.Len()-h.min.Len() == 2 {
		heap.Push(h.min, heap.Pop(h.max))
	} else if h.min.Len()-h.max.Len() == 1 {
		heap.Push(h.max, heap.Pop(h.min))
	}
}

func (h IntMedianHeap) Median() float32 {
	if h.max.Len() == 0 {
		panic("median heap is empty")
	}

	if (h.max.Len()+h.min.Len())%2 == 0 {
		return (float32(h.max.Peek()) + float32(h.min.Peek())) / 2
	} else {
		return float32(h.max.Peek())
	}

}

func (h IntMedianHeap) Update(n interface{}) float32 {
	h.Add(n)
	return h.Median()
}
